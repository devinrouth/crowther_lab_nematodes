# This repository contains the code used for the analyses in the paper "Soil nematode abundance and functional group composition at a global scale"

Paper reference: Van den Hoogen and Geisen et al., 2019, Soil nematode abundance and functional group composition at a global scale, Nature, doi: 10.1038/s41586-019-1418-6.

The data can be downloaded [here](https://doi.org/10.3929/ethz-b-000354035) and [here](https://doi.org/10.3929/ethz-b-000354394).

### The code is organized as follows:
- The folder titled "Nematode_Geospatial" contains code for:
    - Aggregating the raw data by composite pixels (scripts titled Nematode_Sample_Composite_for_Aggregation.js and ETH_Nematode_Aggregate_By_Location.ipynb)
    - Sampling the aggregating pixel locations to retrieve the covariate values and create a "regression matrix" for modelling (script titled Nematode_Sample_Points_for_ClustOfVar.js)
    - Formatting the output data for grid searching and modeling (script titled ChangeCSVColumnNamesBeforeShapefile.ipynb)
    - Grid searching / modeling and model assessment (scripts titled Nematode_Grid_Search_Full.js, Nematode_Grid_Search_Results_Full_Model_Run.ipynb, Nematode_CrossValidate_Ensemble.js, and Nematode_Ensembled_CV_Results.ipynb)
	- Creating the final ensembled maps (scripts titled Nematode_Final_Map_Creation_1.js and Nematode_Final_Map_Creation_2.js)
	- Computing the global abundance values (script titled Nematode_Global_Abundance_Calculations.js)
	- Performing assessment of pixel value variation via bootstrapping (script title Nematode_Biome_BootStrap_StdDev.js)
	- Creating predicted versus observed plots for the final maps (script titled Nematode_Predicted_Vs_Observed_Plots.ipynb)
    
- The folder titled "Nematode_Observations" contains code and data for creating most of the figures of the paper. All maps are output from GEE scripts (see "Nematode_Geospatial")
    - All raw data
    - RMarkdown notebooks for figures and carbon calculations
    - Supplementary Tables 1-8